# unity-physical-liquid

Physical Simulation of liquid/water in Unity3D. The repo contains 4 Unity scenes which show the progression from the simulation of the physics of the water surface with ripples through to caustics and refraction.